vim.o.background = "dark" -- or "light" for light mode
vim.g.gruvbox_material_better_performance = 1
vim.g.gruvbox_material_background = "soft"
vim.g.gruvbox_material_foreground = "original"

vim.cmd([[colorscheme gruvbox-material]])
