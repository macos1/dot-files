vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex, {desc = "Open netrw"})

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv", {desc = "Move multiple lines in visual mode down"})
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv", {desc = "Move multiple lines in visual mode up"})

vim.keymap.set("n", "J", "mzJ`z", {desc = "Bring text from next line to current line"})
vim.keymap.set("n", "<C-d>", "<C-d>zz", { desc = "Center the cursor on down" })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { desc = "Center the cursor on up" })
vim.keymap.set("n", "n", "nzzzv", { desc = "Center the cursor on next search term" })
vim.keymap.set("n", "N", "Nzzzv", { desc = "Center the cursor on prev search term" })

-- persists what's there in the clipboard after pasting in visual mode
vim.keymap.set("x", "p", [["_dP]], { desc = "Doesn't replace clipboard on visual replace" })

-- yank to system clipboard
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]], { desc = "Yank to clipboard" })
vim.keymap.set("n", "<leader>Y", [["+Y]], { desc = "Yank till end to Clipboard" })

-- vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]])

-- remap ctrl+c to esc
vim.keymap.set("i", "<C-c>", "<Esc>", { desc = "Remap ctrl+c to Esc" })

vim.keymap.set("n", "Q", "<nop>", { desc = "Remove remap for Q" })
-- vim.keymap.set("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")

-- format the document
vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, { desc = "[F]ormat document" })

-- quickfix list navigation
-- vim.keymap.set("n", "<leader>K", "<cmd>cnext<CR>zz")
-- vim.keymap.set("n", "<leader>J", "<cmd>cprev<CR>zz")
-- vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
-- vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")

-- rename the word under the cursor
vim.keymap.set(
  "n",
  "<leader>s",
  [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
  { desc = "Rename word under cursor" }
)

-- go to packer config file
vim.keymap.set(
  "n",
  "<leader>vpp",
  "<cmd>e ~/dotfiles/nvim/.config/nvim/lua/sarat/packer.lua<CR>",
  { desc = "Goto Packer file" }
)

vim.keymap.set("n", "<leader><leader>", function()
  vim.cmd("so")
end, { desc = "so" })

-- Use ctrl-[hjkl] to select the active split!
vim.keymap.set("n", "<C-k>", "<cmd>:wincmd k<CR>", { desc = "Top split" })
vim.keymap.set("n", "<C-j>", "<cmd>:wincmd j<CR>", { desc = "Bottom split" })
vim.keymap.set("n", "<C-h>", "<cmd>:wincmd h<CR>", { desc = "Left split" })
vim.keymap.set("n", "<C-l>", "<cmd>:wincmd l<CR>", { desc = "Right split" })

-- buffer navigation
vim.keymap.set("n", "<leader>bn", "<cmd>bnext<CR>", { desc = "[B]uffer [N]ext" })
vim.keymap.set("n", "<leader>bp", "<cmd>bprevious<CR>", { desc = "[B]uffer [P]revious" })
vim.keymap.set("n", "<leader>bd", "<cmd>bdelete<CR>", { desc = "[B]uffer [D]elete" })

-- Telescope
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>pf", builtin.find_files, { desc = "[P]roject [F]ind files" })
vim.keymap.set("n", "<C-p>", builtin.git_files, { desc = "Search git files" })
vim.keymap.set("n", "<leader>ps", function()
  builtin.grep_string({ search = vim.fn.input("Grep > ") })
end, { desc = "[P]roject [S]earch" })
vim.keymap.set("n", "<leader>bs", builtin.buffers, { desc = "[B]uffer [S]how" })
vim.keymap.set("n", "gR", builtin.lsp_references, { desc = "[G]oto [R]eferences" })
vim.keymap.set("n", "gk", builtin.keymaps, { desc = "[G]oto [K]eymaps" })

-- Undo tree
vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle, { desc = "[U]ndo tree toggle" })

-- Harpoon
local mark = require("harpoon.mark")
local ui = require("harpoon.ui")

vim.keymap.set("n", "<leader>a", mark.add_file, { desc = "[A]dd file to harpoon" })
vim.keymap.set("n", "<C-e>", ui.toggle_quick_menu, { desc = "Toggle harpoon menu" })

vim.keymap.set("n", "<leader>h", function()
  ui.nav_file(1)
end, { desc = "harpoon: 1" })
vim.keymap.set("n", "<leader>j", function()
  ui.nav_file(2)
end, { desc = "harpoon: 2" })
vim.keymap.set("n", "<leader>k", function()
  ui.nav_file(3)
end, { desc = "harpoon: 3" })
vim.keymap.set("n", "<leader>l", function()
  ui.nav_file(4)
end, { desc = "harpoon: 4" })

-- Fugitive
vim.keymap.set("n", "<leader>gs", vim.cmd.Git, { desc = "Open fugitive" })
vim.keymap.set("n", "<leader>gd", vim.cmd.Gvdiffsplit, { desc = "[G]it [D]iffsplit" })
vim.keymap.set("n", "<leader>gb", "<cmd>Git blame<CR>", { desc = "[G]it [B]lame" })

-- nvim tree
vim.keymap.set("n", "<leader>e", "<cmd>NvimTreeToggle<CR>", {desc = "Toggle nvim-tree"})

